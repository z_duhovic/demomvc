﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace DemoApl.Models.ViewModels
{
    public class SingleShotPasswordViewModels
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Single Shot Password")]
        public string sUserSingleShotPass { get; set; }
    }
}