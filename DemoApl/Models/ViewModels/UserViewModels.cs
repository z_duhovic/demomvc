﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using FileHelpers;

namespace DemoApl.Models.ViewModels
{
    
    public class UserViewLoginModel
    {

        private UserData CtcsConn = new UserData();

        [Required]
        [Display(Name = "User name")]
        public string sUserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string sUserPass { get; set; }

        public UserData IsUserValid(string _username, string _password)
        {
            var engine = new FileHelperEngine<UserData>();
            
            var result = engine.ReadFile(System.Web.HttpContext.Current.Request.MapPath("~\\DbFiles\\FileIn.txt"));

            UserData oUser = result.FirstOrDefault(user => user.sUserName == _username && user.sUserPass == _password);

            return oUser;

        }

    }

    public class LoggedUser
    {
        [Display(Name = "User name : ")]
        public string sName { get; set; }
        [Display(Name = "User first name : ")]
        public string sFirstName { get; set; }
        public string sUserName { get; set; }
        public string sUserPass { get; set; }
        public string sUserMail { get; set; }
        public string sSingleShotPass { get; set; }
    }
}