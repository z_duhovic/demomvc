﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHelpers;

namespace DemoApl.Models
{
    public class Navbar
    {
        public int Id { get; set; }
        public string nameOption { get; set; }
        public string controller { get; set; }
        public string action { get; set; }
        public string area { get; set; }
        public string imageClass { get; set; }
        public string activeli { get; set; }
        public bool status { get; set; }
        public int parentId { get; set; }
        public bool isParent { get; set; }
    }

    [DelimitedRecord("|")]
    public partial class UserData
    {
        public string sName { get; set; }
        public string sFirstName { get; set; }
        public string sUserName { get; set; }
        public string sUserPass { get; set; }
        public string sUserMail { get; set; }
    }
   
}