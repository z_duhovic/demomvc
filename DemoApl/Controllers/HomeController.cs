﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DemoApl.Models;
using DemoApl.Models.ViewModels;
using DemoApl.DomainFuncs;

namespace DemoApl.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View("Login");
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult LoginWithPass()
        {
            return View();
        }

        public ActionResult WelcomePage()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(Models.ViewModels.UserViewLoginModel user)
        {
            if (ModelState.IsValid)
            {
                //UserData oUser = DemoApp.Models.UserData.Equals();
                var dbStaff = user.IsUserValid(user.sUserName, user.sUserPass);

                if (dbStaff != null)
                {
                    var iLoggedUser = new LoggedUser();

                    FormsAuthentication.SetAuthCookie(user.sUserName, true);

                    iLoggedUser.sFirstName = dbStaff.sUserName;
                    iLoggedUser.sName = dbStaff.sName;
                    iLoggedUser.sUserName = dbStaff.sUserName;
                    iLoggedUser.sUserPass = dbStaff.sUserPass;
                    iLoggedUser.sUserMail = dbStaff.sUserMail;

                    var sPassword = DemoApl.DomainFuncs.SingleShotPasswordGen.Generate(8, 10);
                    iLoggedUser.sSingleShotPass = sPassword;

                    //var mailService = new GmailService();
                    GmailService.GmailUsername = "some@gmail.com";
                    GmailService.GmailPassword = "somepass";
                    

                    GmailService mailService = new GmailService();
                    mailService.ToEmail = dbStaff.sUserMail;
                    mailService.Subject = "DemoApp auth message";
                    mailService.Body = "Authorization password : " + sPassword;
                    mailService.IsHtml = true;
                    mailService.Send();


                    var dtPassTime = DateTime.Now;
                    Session["User"] = iLoggedUser;
                    Session["PassDateTime"] = dtPassTime;

                    return RedirectToAction("LoginWithPass", "Home");
                    
                }
                
            }

            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(user);

        }

        [HttpPost]
        public ActionResult LoginWithPass(Models.ViewModels.SingleShotPasswordViewModels userLogPass)
        {
            if (ModelState.IsValid)
            {

                var user = Session["User"] as LoggedUser;

                if (((DateTime)Session["PassDateTime"]).AddMinutes(1) < DateTime.Now)
                {
                    ModelState.AddModelError("", "Provided password expired.");
                    return View(userLogPass);
                }

                if (string.Equals(user.sSingleShotPass, userLogPass.sUserSingleShotPass, StringComparison.Ordinal) != true)
                {
                    ModelState.AddModelError("", "Provided is incorrect.");
                    return View(userLogPass);
                                        
                }

                return RedirectToAction("WelcomePage", "Home");

            }

            ModelState.AddModelError("", "Invalid data.");
            return View(userLogPass);
            
            
        }

    }
}