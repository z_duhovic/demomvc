﻿using DemoApl.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoApl.DomainFuncs
{
    public class Data
    {
        public IEnumerable<Navbar> navbarItems()
        {
            var menu = new List<Navbar>();
            menu.Add(new Navbar { Id = 1, nameOption = "Login", controller = "Home", action = "Login", status = true, isParent = false, parentId = 0 });
            menu.Add(new Navbar { Id = 2, nameOption = "Logout", controller = "Home", action = "Logout", status = true, isParent = false, parentId = 0 });

            return menu.ToList();
        }
    }
}